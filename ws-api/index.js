const express = require('express');
const cors = require('cors');
const {nanoid} = require('nanoid');
const app = express();

require('express-ws')(app);

const port = 8000;

app.use(cors());



const activeConnection = {};

app.ws('/canvas', (ws, req) => {
    const id = nanoid();
    activeConnection[id] = ws;

    ws.send(JSON.stringify({
        type: 'CONNECTED',
        message: 'Great, you have connected',
    }));

    ws.on('message', msg => {
        const parsedMessage = JSON.parse(msg);
        switch(parsedMessage.type){
            case "CREATE_NEW_LINE":
                Object.keys(activeConnection).forEach(key => {
                    const connection = activeConnection[key];
                    if(key !== id){
                        connection.send(JSON.stringify({
                            type: "NEW_COORDINATE",
                            pixelsArray: parsedMessage.pixelsArray,
                        }))
                    }
                })
                break;
            default:
                console.log('Unknown type.', parsedMessage.type);
        }
    });

    ws.on('close', () => {
        console.log(`Clit disconnected with id=${id}`);
        delete activeConnection[id]
    });
});

app.listen(port, () => {
    console.log(`Server started on ${port}port!`)
})