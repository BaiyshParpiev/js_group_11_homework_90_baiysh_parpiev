import React, {useState, useRef, useEffect} from 'react'


const App = () => {
    const [state, setState] = useState({
        mouseDown: false,
        pixelsArray: []
    });

    const canvas = useRef(null);
    const ws = useRef(null);

    useEffect(() => {
        ws.current = new WebSocket("ws://localhost:8000/canvas")
        ws.current.onmessage = event => {
            const decoded = JSON.parse(event.data);
            if(decoded.type === 'NEW_COORDINATE'){
                decoded.pixelsArray.map(p => {
                    const context = canvas.current.getContext('2d');
                    context.beginPath();
                    context.arc(p.x, p.y, 10, 0, 2 * Math.PI, false);
                    context.fillStyle = 'red';
                    context.fill();
                    context.lineWidth = 5;
                    context.strokeStyle = 'green';
                    return context.stroke();
                })
            }
            if(decoded.type === "CONNECTED"){
               console.log(decoded.message)
            }
        };

    }, []);

    const canvasMouseMoveHandler = event => {
        if (state.mouseDown) {
            event.persist();
            const clientX = event.clientX;
            const clientY = event.clientY;
            setState(prevState => {
                return {
                    ...prevState,
                    pixelsArray: [...prevState.pixelsArray, {
                        x: clientX,
                        y: clientY
                    }]
                };
            });

            const context = canvas.current.getContext('2d');
            context.beginPath();
            context.arc(event.clientX, event.clientY, 10, 0, 2 * Math.PI, false);
            context.fillStyle = 'red';
            context.fill();
            context.lineWidth = 5;
            context.strokeStyle = 'green';
            return context.stroke();
        }
    };

    const mouseDownHandler = e => {
        e.preventDefault();
        setState({...state, mouseDown: true});
    };

    const mouseUpHandler = e => {
        e.preventDefault();
        ws.current.send(JSON.stringify({
            type: "CREATE_NEW_LINE",
            pixelsArray: state.pixelsArray,
        }));
        setState({...state, mouseDown: false, pixelsArray: []});
    };



    return (
        <div>
            <canvas
                ref={canvas}
                style={{border: '1px solid black'}}
                width={800}
                height={600}
                onMouseDown={mouseDownHandler}
                onMouseUp={mouseUpHandler}
                onMouseMove={canvasMouseMoveHandler}
            />
        </div>
    );
};

export default App;